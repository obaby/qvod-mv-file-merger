#Qvod (!mv)文件合并器

>如果用过手机上的快播（Android）会发现看完的影片最终会是一些!mv文件，如果要在电脑上看就会变得灰常蛋疼
>当然如果只是看一次，那么至于最终会成为什么样子就不关键了。关键的是还想再看第二次的。

>这个东西就是用来在电脑上合并文件的，当然可以用xcopy命令。如果不喜欢那个东西就可以用这个小软件了，灰常简单。


##屏幕截图
![screenshot](http://code.h4ck.org.cn/qvod-mv-file-merger/raw/c4881c36aef2075f574fe696fac5cd70f676e281/screen.png)

##

##视频截图
![video](http://code.h4ck.org.cn/qvod-mv-file-merger/raw/ccceaeb50a4d526916b91fab7e1fe8947433f2e5/video.png)

测试的合并文件为1.5G，150个文件，影片时长3小时，如果出现不兼容的情况请自行修改代码！

##使用方法
选择要合并文件的第一个文件，程序会自动会自动填写目标文件名。选择完之后点“开始合体”就行鸟。 就这么简单


###火星信息安全研究院
http://www.h4ck.org.cn
