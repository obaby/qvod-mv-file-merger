unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SkinCtrls, SkinData, DynamicSkinForm, spSkinShellCtrls,
  StdCtrls, Mask, SkinBoxCtrls, SkinExCtrls, spMessages;

type
  TForm1 = class(TForm)
    spDynamicSkinForm1: TspDynamicSkinForm;
    spSkinFrame1: TspSkinFrame;
    spSkinData1: TspSkinData;
    spCompressedStoredSkin1: TspCompressedStoredSkin;
    spCompressedSkinList1: TspCompressedSkinList;
    spResourceStrData1: TspResourceStrData;
    spSkinButton_combine: TspSkinButton;
    spSkinShadowLabel_1stFile: TspSkinShadowLabel;
    spSkinShadowLabel_dstFile: TspSkinShadowLabel;
    spSkinEdit_1stFile: TspSkinEdit;
    spSkinEdit_dstFile: TspSkinEdit;
    spSkinSpeedButton_1stFile: TspSkinSpeedButton;
    spSkinSpeedButton_dstFile: TspSkinSpeedButton;
    spSkinStatusBar1: TspSkinStatusBar;
    spSkinOpenDialog1: TspSkinOpenDialog;
    spSkinSaveDialog1: TspSkinSaveDialog;
    spSkinMessage1: TspSkinMessage;
    spSkinStatusPanel1: TspSkinStatusPanel;
    spSkinStatusPanel2: TspSkinStatusPanel;
    spSkinLinkLabel1: TspSkinLinkLabel;
    procedure spSkinButton_combineClick(Sender: TObject);
    procedure spSkinSpeedButton_1stFileClick(Sender: TObject);
    procedure spSkinSpeedButton_dstFileClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  hThread:Thandle;//定义一个句柄
  ThreadID:DWord;

implementation

var

  path: string;

{$R *.dfm}

//分割文件的函数
{参数 1 是要分割的文件名; 参数 2 是要风格文件的大小, 单位是 kb}
{分割后的文件名扩展名用序号替换}
function splitfile(const filename: string; size: cardinal): boolean;
var
  fstream: tfilestream;    {原始文件}
  tostream: tmemorystream; {分文件}
  p,i: integer;            {p 记录当前指针位置; i 记录这是第几个分的文件}
begin
  result := false;

  size := size * 1024;     {把大小的单位转换为字节}

  fstream := tfilestream.create(filename, fmopenread);
  p := 0;
  i := 0;

  tostream := tmemorystream.create;

  while p < fstream.size do
  begin
    tostream.clear;        {清空上次数据}
    fstream.position := p; {放好指针位置}

    if fstream.size-p < size then size := fstream.size-p; {最后一个时, 有多少算多少}

    tostream.copyfrom(fstream, size); {复制}
    tostream.savetofile(filename + '.' + inttostr(i));        {保存}
    inc(i);
    p := p + size;
  end;

  fstream.free;
  tostream.free;
  result := true;
end;


//合并文件, 参数是其中一个分文件名
function mergefile(const sourcefilename: string ;destfilename :string): boolean;
var
  ms: tmemorystream; {读取分文件}
  fs: tfilestream;   {合并后的文件}
  tmppath : string;
  i: integer;
begin

  tmppath := Copy(sourcefilename,0,Length(sourcefilename)- 5);
  //ShowMessage(tmppath);
  i := 0;

  ms := tmemorystream.create;
  fs := tfilestream.create(destfilename, fmcreate);

  while fileexists(tmppath + inttostr(i) + '.!mv') do
  begin
    ms.loadfromfile(tmppath + inttostr(i) + '.!mv');
    fs.copyfrom(ms, 0); {tfilestream 不需要 setsize; 但如果用 tmemorystream 就需要}
    inc(i);
    Form1.spSkinStatusPanel1.Caption := '状态:正在合并第' + IntToStr(i) + '个';
  end;

  ms.free;
  fs.free;
end;

procedure ThreadMergeFile();
begin
  mergefile(Form1.spSkinEdit_1stFile.Text,Form1.spSkinEdit_dstFile.Text);
  Form1.spSkinStatusPanel1.Caption := '状态：合并完成！';
end;

procedure TForm1.spSkinButton_combineClick(Sender: TObject);
begin
  if not (FileExists(spSkinEdit_1stFile.Text)) then begin
     spskinmessage1.MessageDlg('哥哥，记得选择原始文件的第一个哦！',mtWarning,[mbYes],0);
     Exit;
  end;
  //mergefile(spSkinEdit_1stFile.Text,spSkinEdit_dstFile.Text);
  hthread:=CreateThread(nil,0,@ThreadMergeFile,nil,0,ThreadID);
end;

procedure TForm1.spSkinSpeedButton_1stFileClick(Sender: TObject);
begin
    spSkinOpenDialog1.Execute;
    if not ( FileExists( spSkinOpenDialog1.FileName) ) then begin
      spSkinMessage1.MessageDlg('你确定这个文件存在么？哥哥。',mtError,[mbYes],0);
      Exit;
    end;
    spSkinEdit_1stFile.Text := spSkinOpenDialog1.FileName;
    spSkinEdit_dstFile.Text := Copy(spSkinEdit_1stFile.Text,0,Length(spSkinEdit_1stFile.Text)-6);
end;

procedure TForm1.spSkinSpeedButton_dstFileClick(Sender: TObject);
begin
   spSkinSaveDialog1.Execute;
   if spSkinSaveDialog1.FileName = '' then begin
     spSkinMessage1.MessageDlg('请输入文件名啦，哥哥',mtWarning,[mbYes],0);
     Exit;
   end;
   spSkinEdit_dstFile.Text := spSkinSaveDialog1.FileName;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  TerminateThread(hThread,0); //　终止线程
end;

end.
